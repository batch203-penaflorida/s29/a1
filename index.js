// Find users with letter s in ther first name or d in ther last name

db.users.find(
  {
    $or: [
      {
        firstName: { $regex: "S", $options: "$i" },
      },
      {
        lastName: { $regex: "D", $options: "$i" },
      },
    ],
  },
  {
    firstName: 1,
    lastName: 1,
    _id: 0,
  }
);

// department - HR && age gte 70
db.users.find({
  $and: [{ age: { $lte: 30 } }, { lastName: { $regex: "e", $options: "$i" } }],
});

db.users.find({
  $and: [{ department: "HR" }, { age: { $gte: 70 } }],
});
